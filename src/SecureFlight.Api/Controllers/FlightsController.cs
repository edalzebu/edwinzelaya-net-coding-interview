﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController(IFlightService flightService, IMapper mapper)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpGet]
    [Route("{origin}/{destination}")]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> GetFlightsByOriginAndDestination(string origin, string destination)
    {
        var flightsResult = await flightService.FilterAsync(f => f.OriginAirport == origin &&
        f.DestinationAirport == destination);

        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flightsResult);
    }

    [HttpPut]
    [Route("{flightId:long}/passenger/{passengerId}")]
    public async Task<IActionResult> AddPassengerToFlight(long flightId, string passengerId)
    {
        var addPassengerResult = await flightService.AddPassengerToFlight(flightId, passengerId);

        return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(addPassengerResult);
    }

}