﻿using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services
{
    public class FlightService(IRepository<Flight> flightRepository, IRepository<Passenger> passengerRepository) : BaseService<Flight>(flightRepository), IFlightService
    {
        public async Task<OperationResult<Flight>> AddPassengerToFlight(long flightId, string passengerId)
        {
            var flightResult = await FindAsync(flightId);

            if (!flightResult.Succeeded) return flightResult;

            var passenger = await passengerRepository.GetByIdAsync(passengerId);

            if (passenger is null) return OperationResult<Flight>.NotFound($"Passenger with id {passengerId} was not found");

            var flight = flightResult.Result;

            flight.AddPassenger(passenger);

            var numberOfRowsModified = await flightRepository.SaveChangesAsync();

            return numberOfRowsModified > 0 ?
                OperationResult<Flight>.Success(flight) :
                OperationResult<Flight>.Error("Something went wrong, passenger wasnt added to the flight.");
        }
    }
}
