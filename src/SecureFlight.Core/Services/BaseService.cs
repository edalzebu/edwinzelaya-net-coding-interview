﻿using SecureFlight.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SecureFlight.Core.Services;

public class BaseService<TEntity>(IRepository<TEntity> repository) : IService<TEntity>
    where TEntity : class
{
    public async Task<OperationResult<IReadOnlyList<TEntity>>> GetAllAsync()
    {
        return OperationResult<IReadOnlyList<TEntity>>.Success(await repository.GetAllAsync());
    }

    public async Task<OperationResult<IReadOnlyList<TEntity>>> FilterAsync(Expression<Func<TEntity, bool>> predicate)
    {
        return OperationResult<IReadOnlyList<TEntity>>.Success(await repository.FilterAsync(predicate));
    }

    public async Task<OperationResult<TEntity>> FindAsync(params object[] keyValues)
    {
        var entity = await repository.GetByIdAsync(keyValues);
        return entity is null ?
            OperationResult<TEntity>.NotFound($"Entity with key values {string.Join(", ", keyValues)} was not found") :
            OperationResult<TEntity>.Success(entity);
    }

    public async Task<OperationResult<TEntity>> UpdateAsync(TEntity airport)
    {
        repository.Update(airport);

        var numberOfRowsAffected = await repository.SaveChangesAsync();

        return numberOfRowsAffected > 0 ?
            OperationResult<TEntity>.Success(airport) :
            OperationResult<TEntity>.Error("No rows were affected");
    }
}