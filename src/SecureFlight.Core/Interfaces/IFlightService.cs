﻿using SecureFlight.Core.Entities;
using System.Threading.Tasks;

namespace SecureFlight.Core.Interfaces;

public interface IFlightService : IService<Flight>
{
    Task<OperationResult<Flight>> AddPassengerToFlight(long flightId, string passengerId);
}
