﻿using System.Collections.Generic;

namespace SecureFlight.Core.Entities;

public class Airport
{
    public Airport()
    {
    }
    public Airport(string code, string name, string city, string country)
    {
        Code = code;
        Name = name;
        City = city;
        Country = country;
    }

    public string Code { get; set; }

    public string Name { get; set; }

    public string City { get; set; }

    public string Country { get; set; }

    public ICollection<Flight> OriginFlights { get; set; } = new HashSet<Flight>();

    public ICollection<Flight> DestinationFlights { get; set; } = new HashSet<Flight>();

    public static Airport Create(string code, string name, string city, string country)
    {
        return new Airport(code, name, city, country);
    }
}