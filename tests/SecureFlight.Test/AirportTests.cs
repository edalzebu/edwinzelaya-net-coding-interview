using SecureFlight.Core.Entities;
using SecureFlight.Core.Services;
using SecureFlight.Infrastructure.Repositories;
using System.Threading.Tasks;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        [Fact]
        public async Task Should_Update_Airports_Name_City_Country()
        {
            //Arrange
            var testingContext = new SecureFlightDatabaseTestContext();
            testingContext.CreateDatabase();
            var repository = new BaseRepository<Airport>(testingContext);
            //var mockRepository = new Mock<IRepository<Airport>>();
            var service = new BaseService<Airport>(repository);
            //TODO: Add test code here
            //Act
            var updateToAirport = Airport.Create("JFK", "John F. Kennedy International Airport2", "New York3", "United States4");

            var updateResult = await service.UpdateAsync(updateToAirport);


            //Assert

            Assert.True(updateResult.Succeeded);

            var airport = updateResult.Result;

            Assert.Equal("JFK", airport.Code);
            Assert.Equal("John F. Kennedy International Airport2", airport.Name);
            Assert.Equal("New York3", airport.City);
            Assert.Equal("United States4", airport.Country);

            testingContext.DisposeDatabase();
        }
    }
}
